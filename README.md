kubeconfig not created in the original git repository, here is an example of how to doit exctrated from google


```js
data "aws_eks_cluster" "example" {
  name = "example"
}

output "endpoint" {
  value = data.aws_eks_cluster.example.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = data.aws_eks_cluster.example.certificate_authority[0].data
}

# Only available on Kubernetes version 1.13 and 1.14 clusters created or upgraded on or after September 3, 2019.
output "identity-oidc-issuer" {
  value = data.aws_eks_cluster.example.identity[0].oidc[0].issuer
}
```


Cluster created 
![[Pasted image 20240207182011.png]]

## Ansible 

Example of deploy-to-k8s.yaml

```yaml
---
- name: Deploy app in new namespace
  hosts: localhost
  tasks:
    - name: Create a k8s namespace
      k8s:
        name: my-app
        api_version: v1
        kind: Namespace
        state: present        
    - name: Deploy nginx app
      k8s:
        src: /home/radisys/devops_training/capstone-project-2/nginx-config.yaml
        state: present
        namespace: my-app

```

